#!/usr/bin/python
#-*- coding: utf-8 -*-

import RPi.GPIO as GPIO 
import Base

class SensorAlarme(Base.Base):
    
    def __init__(self, id, numeroGPIO, ativo, nome):
        self.id         = id
        self.numeroGPIO = numeroGPIO
        self.ativo      = ativo
        self.nome       = nome
        
        self.configurar()
            
    def configurar(self):
        GPIO.setmode(GPIO.BCM) 
        GPIO.setup(int(self.numeroGPIO), GPIO.IN)
    
    def lerStatus(self):
        return GPIO.input(self.numeroGPIO)
    
    def gravarRegistroBanco(self):
        sql = "update SensorAlarme set Nome = '{nomeSensor}', Ativo = {ativo} where Id = {idSensor}"
        sql = sql.format(nomeSensor = self.nome, ativo = self.ativo, idSensor = self.id)
        
        return self.executarComando(sql)