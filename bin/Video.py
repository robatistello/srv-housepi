#!/usr/bin/python
#-*- coding: utf-8 -*-

import Funcoes
from pyomxplayer import OMXPlayer
from pprint import pprint
from os import listdir
from os.path import isfile, join
    
class Video(object):
    
    def getCaminhoVideos(self): 
        return Funcoes.lerConfiguracaoIni("CaminhoVideos")
        
    def getListaVideo(self):
        arquivos = [f for f in listdir(self.getCaminhoVideos()) if isfile(join(self.getCaminhoVideos(),f))]
        return arquivos
        
    def pause(self):
        try:
            self.omx.toggle_pause()
        except:
            print "Não está executando!"
    
    def stop(self):
        try:
            self.omx.stop()            
        except:
            print "Não está executando!"

    def playNome(self, valor):
        self.stop()
        self.omx = OMXPlayer(self.getCaminhoVideos() + valor.replace(' ', '\ '))
        pprint(self.omx.__dict__)
    
    def avancar(self):
        try:
            self.omx.forward()
        except:
            print "Não está executando!"
    
    def retroceder(self):
        try:
            self.omx.backward()
        except:
            print "Não está executando!"