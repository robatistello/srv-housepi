-- Adminer 4.2.4 SQLite 3 dump

DROP TABLE IF EXISTS "Agendamento";
CREATE TABLE "Agendamento" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "DataHoraInicial" numeric NOT NULL,
  "DataHoraFinal" numeric NOT NULL,
  "Ativo" integer NOT NULL DEFAULT '1',
  "Nome" text NOT NULL,
  "Alarme" integer NOT NULL DEFAULT '0'
);


DROP TABLE IF EXISTS "Camera";
CREATE TABLE "Camera" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "Nome" text NOT NULL,
  "Device" text NOT NULL,
  "Porta" integer NOT NULL,
  "Resolucao" text NOT NULL DEFAULT '320x240',
  "Frames" integer NOT NULL DEFAULT '3',
  "YUV" integer NOT NULL DEFAULT '1'
);

INSERT INTO "Camera" ("Id", "Nome", "Device", "Porta") VALUES (1,	'Camera1',	'/dev/video0',	2343);

DROP TABLE IF EXISTS "ConfiguracaoAlarme";
CREATE TABLE "ConfiguracaoAlarme" (
  "Id" int(11) NOT NULL,
  "EnviarEmail" int(11) NOT NULL,
  "UsarSirene" int(11) NOT NULL,
  "DesligarDisparoConsecutivo" int(11) NOT NULL,
  "TempoDisparo" int(11) NOT NULL,
  "StatusAlarme" int(11) NOT NULL DEFAULT '0',
  "StatusPanico" int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY ("Id")
);

INSERT INTO "ConfiguracaoAlarme" ("Id", "EnviarEmail", "UsarSirene", "DesligarDisparoConsecutivo", "TempoDisparo", "StatusAlarme", "StatusPanico") VALUES (1,	'0',	1,	1,	120,	'0',	'0');

DROP TABLE IF EXISTS "ConfiguracaoEmail";
CREATE TABLE "ConfiguracaoEmail" (
  "Id" int(11) NOT NULL,
  "Remetente" varchar(100) DEFAULT NULL,
  "Senha" varchar(60) DEFAULT NULL,
  "Destinatario" varchar(500) DEFAULT NULL,
  "ServidorSMTP" varchar(100) DEFAULT NULL,
  "PortaSMTP" int(11) DEFAULT NULL,
  PRIMARY KEY ("Id")
);

INSERT INTO "ConfiguracaoEmail" ("Id", "Remetente", "Senha", "Destinatario", "ServidorSMTP", "PortaSMTP") VALUES (1,	'',	'',	'',	'',	587);

DROP TABLE IF EXISTS "DiaAgendamento";
CREATE TABLE "DiaAgendamento" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "IdAgendamento" integer NOT NULL,
  "Dia" integer NOT NULL,
  FOREIGN KEY ("IdAgendamento") REFERENCES "Agendamento" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "DiaAgendamento_Dias_do_Agendamento" ON "DiaAgendamento" ("IdAgendamento");


DROP TABLE IF EXISTS "DisparoAlarme";
CREATE TABLE "DisparoAlarme" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "IdSensorAlarme" integer NOT NULL,
  "DataHora" numeric NOT NULL,
  FOREIGN KEY ("IdSensorAlarme") REFERENCES "SensorAlarme" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "DisparoAlarme_Sensor_Disparo" ON "DisparoAlarme" ("IdSensorAlarme");


DROP TABLE IF EXISTS "RFID";
CREATE TABLE "RFID" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "Tag" text NOT NULL
);


DROP TABLE IF EXISTS "Rele";
CREATE TABLE "Rele" (
  "Id" int(11) NOT NULL,
  "Nome" varchar(60) DEFAULT NULL,
  "Status" int(11) NOT NULL,
  "NumeroGPIO" int(11) NOT NULL,
  "Ativo" int(11) NOT NULL,
  PRIMARY KEY ("Id")
);

INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES ('0',	'Rele 1',	'0',	'0',	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (1,	'Rele 2',	'0',	1,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (2,	'Rele 3',	'0',	2,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (3,	'Rele 4',	'0',	3,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (4,	'Rele 5',	'0',	4,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (5,	'Rele 6',	'0',	5,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (6,	'Rele 7',	'0',	6,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (7,	'Rele 8',	'0',	7,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (8,	'Rele 9',	'0',	8,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (9,	'Rele 10',	'0',	9,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (10,	'12 Volts - GPB2',	'0',	10,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (11,	'12 Volts - GPB3',	'0',	11,	1);
INSERT INTO "Rele" ("Id", "Nome", "Status", "NumeroGPIO", "Ativo") VALUES (12,	'12 Volts - GPB4',	'0',	12,	1);

DROP TABLE IF EXISTS "ReleAgendamento";
CREATE TABLE "ReleAgendamento" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "IdAgendamento" integer NOT NULL,
  "IdRele" integer NOT NULL,
  FOREIGN KEY ("IdAgendamento") REFERENCES "Agendamento" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY ("IdRele") REFERENCES "Rele" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "ReleAgendamento_Reles" ON "ReleAgendamento" ("IdRele");


DROP TABLE IF EXISTS "SensorAlarme";
CREATE TABLE "SensorAlarme" (
  "Id" int(11) NOT NULL,
  "Nome" varchar(60) DEFAULT NULL,
  "Ativo" int(11) NOT NULL,
  "NumeroGPIO" int(11) NOT NULL,
  PRIMARY KEY ("Id")
);

INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES ('0',	'Sensor 0',	'0',	17);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (1,	'Sensor 1',	'0',	18);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (2,	'Sensor 2',	'0',	27);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (3,	'Sensor 3',	'0',	22);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (4,	'Sensor 4',	'0',	23);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (5,	'Sensor 5',	'0',	24);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (6,	'Sensor 6',	'0',	25);
INSERT INTO "SensorAlarme" ("Id", "Nome", "Ativo", "NumeroGPIO") VALUES (7,	'Sensor 7',	'0',	4);

DROP TABLE IF EXISTS "Usuario";
CREATE TABLE "Usuario" (
  "Id" int(11) NOT NULL,
  "Usuario" varchar(60) NOT NULL,
  "Senha" varchar(60) NOT NULL,
  PRIMARY KEY ("Id")
);

INSERT INTO "Usuario" ("Id", "Usuario", "Senha") VALUES (1,	'admin',	'admin');


INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('Agendamento',	NULL);
INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('Camera',	'1');
INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('DiaAgendamento',	'0');
INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('DisparoAlarme',	'0');
INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('RFID',	'0');
INSERT INTO "sqlite_sequence" ("name", "seq") VALUES ('ReleAgendamento',	'0');

-- 
